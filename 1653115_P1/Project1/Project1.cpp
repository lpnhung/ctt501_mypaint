// Project1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Project1.h"
#include <commctrl.h>	
#include <commdlg.h>

#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hWndClient, hWndFrame, hToolBarWnd;
int nDraw = 0;

// Forward declarations of functions included in this code module:
void                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    DrawProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    MDICloseProc(HWND, LPARAM);
void CreateToolbar(HWND hWnd);
void AddUserButton();
void ToolbarNotifyHandle(LPARAM lParam);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PROJECT1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJECT1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hWndClient, &msg) && !TranslateAccelerator(hWndFrame, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
void MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJECT1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PROJECT1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    RegisterClassExW(&wcex);

	wcex.lpszClassName = L"Draw";
	wcex.lpfnWndProc = DrawProc;
	wcex.hCursor = LoadCursor(nullptr, IDC_CROSS);
	wcex.lpszMenuName = NULL;
	RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWndFrame = CreateWindowW(szWindowClass, TEXT("MyPaint"), WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWndFrame)
   {
      return FALSE;
   }

   ShowWindow(hWndFrame, nCmdShow);
   UpdateWindow(hWndFrame);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	TCHAR msg[128];
	case WM_CREATE:
		{
			CLIENTCREATESTRUCT ccs;
			ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
			ccs.idFirstChild = 60000;
			hWndClient = CreateWindow(L"MDICLIENT", (LPCTSTR)NULL, WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL, 0, 0, 0, 0, hWnd, NULL, hInst, (LPVOID)&ccs);
			ShowWindow(hWndClient, SW_SHOW);
			CreateToolbar(hWnd);
			AddUserButton();
		}
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
			HMENU hMenu = GetSubMenu(GetMenu(hWndFrame), 1);
            // Parse the menu selections:
            switch (wmId)
            {
			case ID_FILE_NEW:
				{
					WCHAR title[MAX_LOADSTRING];
					wsprintf(title, L"Noname-%d.drw", nDraw);
					MDICREATESTRUCT mdiCreate;
					mdiCreate.szClass = L"Draw";
					mdiCreate.hOwner = hInst;
					mdiCreate.szTitle = title;
					mdiCreate.style = 0;
					mdiCreate.cx = CW_USEDEFAULT;
					mdiCreate.cy = CW_USEDEFAULT;
					mdiCreate.x = CW_USEDEFAULT;
					mdiCreate.y = CW_USEDEFAULT;
					mdiCreate.lParam = NULL;

					SendMessage(hWndClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
				}
				break;
			case ID_FILE_OPEN:
				wsprintf(msg, L"Ban da chon Open");
				MessageBox(hWnd, msg, L"Open", MB_OK);
				break;
			case ID_FILE_SAVE:
				wsprintf(msg, L"Ban da chon Save");
				MessageBox(hWnd, msg, L"Save", MB_OK);
				break;
			case ID_FILE_EXIT:
				PostQuitMessage(0);
				break;
			case ID_DRAW_COLOR:
				{
					CHOOSECOLOR cc;
					COLORREF acrCustClr[16];
					DWORD rgbCur = RGB(255, 0, 0);

					ZeroMemory(&cc, sizeof(CHOOSECOLOR));
					cc.lStructSize = sizeof(CHOOSECOLOR);
					cc.hwndOwner = hWnd;
					cc.lpCustColors = (LPDWORD)acrCustClr;
					cc.rgbResult = rgbCur;
					cc.Flags = CC_FULLOPEN | CC_RGBINIT;
					if (ChooseColor(&cc))
					{
						HBRUSH hBrush;
						hBrush = CreateSolidBrush(cc.rgbResult);
						rgbCur = cc.rgbResult;
					}
				}
				break;
			case ID_DRAW_FONT:
				{
					CHOOSEFONT cf;
					LOGFONT lf;

					ZeroMemory(&cf, sizeof(CHOOSEFONT));
					cf.lStructSize = sizeof(CHOOSEFONT);
					cf.hwndOwner = hWnd;
					cf.lpLogFont = &lf;
					cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
					if (ChooseFont(&cf))
					{
					}
				}
				break;
			case ID_DRAW_LINE:
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
				break;
			case ID_DRAW_RECTANGLE:
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_CHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
				break;
			case ID_DRAW_ELLIPSE:
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_CHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
				break;
			case ID_DRAW_TEXT:
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_TEXT, MF_CHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_SELECTOBJECT, MF_UNCHECKED | MF_BYCOMMAND);
				break;
			case ID_DRAW_SELECTOBJECT:
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_TEXT, MF_UNCHECKED | MF_BYCOMMAND);
				CheckMenuItem(hMenu, ID_DRAW_SELECTOBJECT, MF_CHECKED | MF_BYCOMMAND);
				break;
			case ID_WINDOW_TIDE:
				SendMessage(hWndClient, WM_MDITILE, MDITILE_SKIPDISABLED, 0L);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hWndClient, WM_MDICASCADE, MDITILE_SKIPDISABLED, 0L);
				break;
			case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hWndClient, (WNDENUMPROC)MDICloseProc, 0L);
				break;
            default:
                return DefFrameProc(hWnd, hWndClient, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_SIZE:
		{
			int w, h;
			w = LOWORD(lParam);
			h = HIWORD(lParam);
			MoveWindow(hWndClient, 0, 29, w, h, TRUE);
		}
		break;
	case WM_NOTIFY:
		ToolbarNotifyHandle(lParam);
		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
		return DefFrameProc(hWnd, hWndClient, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK DrawProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		++nDraw;
		break;
	case WM_MDIACTIVATE:
		{
			HMENU hMenu = GetMenu(hWndFrame);
			DrawMenuBar(hWndFrame);
		}
		break;
	case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
			default:
				return DefMDIChildProc(hWnd, message, wParam, lParam);
			}
		}
		break;
	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code that uses hdc here...
			EndPaint(hWnd, &ps);
		}
		break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK MDICloseProc(HWND hChildWnd, LPARAM lParam)
{
	SendMessage(hWndClient, WM_MDIDESTROY, (WPARAM)hChildWnd, 0L);
	return 1;
}

void CreateToolbar(HWND hWnd)
{
	InitCommonControls();
	TBBUTTON tbButtons[] =
	{
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	hToolBarWnd = CreateToolbarEx(hWnd, WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS, ID_TOOLBAR,
									sizeof(tbButtons) / sizeof(TBBUTTON),
									HINST_COMMCTRL,
									0,
									tbButtons,
									sizeof(tbButtons) / sizeof(TBBUTTON),
									BUTTON_WIDTH,
									BUTTON_HEIGHT,
									IMAGE_WIDTH,
									IMAGE_HEIGHT,
									sizeof(TBBUTTON));

	CheckMenuItem(GetMenu(hWnd), IDC_PROJECT1, MF_CHECKED | MF_BYCOMMAND);
}

void AddUserButton()
{
	TBBUTTON tbButtons[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, ID_DRAW_LINE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 4, ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};

	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1 };
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP), (LPARAM)(LPTBADDBITMAP)&tbBitmap);

	tbButtons[1].iBitmap += idx;
	tbButtons[2].iBitmap += idx;
	tbButtons[3].iBitmap += idx;
	tbButtons[4].iBitmap += idx;
	tbButtons[5].iBitmap += idx;

	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON), (LPARAM)(LPTBBUTTON)&tbButtons);
}

void ToolbarNotifyHandle(LPARAM lParam)
{
	LPTOOLTIPTEXT   lpToolTipText;
	WCHAR			szToolTipText[TOOL_TIP_MAX_LEN]; 	
				
	lpToolTipText = (LPTOOLTIPTEXT)lParam;

	if (lpToolTipText->hdr.code == TTN_NEEDTEXT)
	{
		LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, TOOL_TIP_MAX_LEN);

		lpToolTipText->lpszText = szToolTipText;
	}
}

